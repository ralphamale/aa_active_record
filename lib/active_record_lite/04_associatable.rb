require_relative '03_searchable'
require 'active_support/inflector'

# Phase IVa
class AssocOptions
  attr_accessor(
  :foreign_key,
  :class_name,
  :primary_key,
  )




  def model_class
    self.class_name.constantize
  end

  def table_name    #
    @class_name.constantize.table_name
  end
end

class BelongsToOptions < AssocOptions
  def initialize(name, options = {})
    defaults = {:class_name => name.to_s.camelcase.singularize,
      :foreign_key => "#{name.to_s}_id".to_sym,
      :primary_key => :id }

      options = defaults.merge(options)
      options.each {|key, value| instance_variable_set("@#{key}", value) }
    end

    def model_class
      self.class_name.constantize
    end

    def table_name
      self.class_name.constantize.table_name
      #
    end

  end

  class HasManyOptions < AssocOptions
    def initialize(name, self_class_name, options = {})
      defaults = {:class_name => name.to_s.camelcase.singularize,
        :foreign_key => "#{self_class_name.underscore}_id".to_sym,
        :primary_key => :id }

        #
        # options = HasManyOptions.new("cats", "Human")
        #
        # expect(options.foreign_key).to eq(:human_id)
        # expect(options.class_name).to eq("Cat")
        # expect(options.primary_key).to eq(:id)


        options = defaults.merge(options)
        options.each {|key, value| instance_variable_set("@#{key}", value) }
      end
    end

    module Associatable
      # Phase IVb   #association name
      def belongs_to(name, options = {})


        options = BelongsToOptions.new(name, options)

        assoc_options[name] = options

        define_method(name) do
          options.model_class.where({options.primary_key => self.send(options.foreign_key)}).first



        end


      end



      def has_many(name, options = {})

        options = HasManyOptions.new(name, "#{self}", options)


        define_method(name) do
          options.class_name.constantize.where({options.foreign_key => self.send(options.primary_key)})
        end


      end

      def assoc_options
          @assoc_options ||= {}
      end
    end

    class SQLObject
      # Mixin Associatable here...
      extend Associatable
    end
